/* eslint-disable */
const path = require("path");

module.exports = {
  "env": {
    "browser": true,
    "es6": true,
    "node": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:import/errors",
    "plugin:import/warnings",
    "plugin:import/typescript",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
    // "next/core-web-vitals"
  ],
  "globals": {
    "Atomics": "readonly",
    "SharedArrayBuffer": "readonly"
  },
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true
    },
    "project": "tsconfig.json",
    "ecmaVersion": 2018,
    "sourceType": "module",
    "createDefaultProgram": true
  },
  "plugins": [
    "@typescript-eslint",
    "@typescript-eslint/tslint",
    "@373/restricted-paths", // TODO: vhodne presunout do samostatneho repozitare, nebo alespon submodulu
    "import",
    "no-null",
    "prefer-arrow",
    "react"
  ],
  "rules": {
    "@typescript-eslint/array-type": ["warn", { "default": "array" }],
    "@typescript-eslint/ban-tslint-comment": "warn",
    // "@typescript-eslint/class-name-casing": "error",
    "@typescript-eslint/consistent-type-definitions": ["warn", "interface"],
    "@typescript-eslint/explicit-member-accessibility": ["warn", { "accessibility": "explicit" }],
    "@typescript-eslint/indent": "off", // zlobi
    // "@typescript-eslint/interface-name-prefix": "error",
    "@typescript-eslint/member-ordering": "warn",
    "@typescript-eslint/member-delimiter-style": [
      "warn",
      {
        "multiline": {
          "delimiter": "none",
          "requireLast": false
        },
        "singleline": {
          "delimiter": "comma",
          "requireLast": false
        },
        "overrides": {
          "interface": {
            "multiline": {
              "delimiter": "none"
            },
            "singleline": {
              "delimiter": "comma"
            }
          }
        }
      }
    ],
    // "@typescript-eslint/no-empty-function": "warn",
    "@typescript-eslint/no-require-imports": "error",
    "@typescript-eslint/no-unused-expressions": ["warn", { "allowShortCircuit": true }],
    "@typescript-eslint/no-use-before-define": "off",
    "@typescript-eslint/no-var-requires": "error",
    "@typescript-eslint/prefer-for-of": "warn",
    "@typescript-eslint/quotes": ["warn", "double", { "avoidEscape": true }],
    "@typescript-eslint/semi": ["warn", "always"],
    "@typescript-eslint/type-annotation-spacing": "error",
    "@373/restricted-paths/no-restricted-paths": "off",
    "brace-style": ["warn", "1tbs"],
    "camelcase": "error",
    "comma-dangle": "warn",
    "complexity": ["error", { "max": 20 }],
    "constructor-super": "error",
    "curly": "off",
    "eol-last": "off",
    "id-blacklist": ["error", "any", "Number", "number", "String", "string", "Boolean", "boolean", "Undefined", "undefined"],
    "id-match": "error",
    "import/named": "off", // zlobi to nad @blueprintjs
    "import/namespace": "off", // zlobi to nad moment
    "import/no-cycle": ["error", { "maxDepth": 15 }], // expensive operation
    "import/no-default-export": "error",
    "import/no-unresolved": "off", // tohle za me resit typescript, tak proc se tim obtezovat
    "import/no-extraneous-dependencies": ["error"],
    "indent": "off",
    "max-classes-per-file": ["error", 1],
    "max-len": ["warn", { "code": 175 }],
    "max-lines": ["warn", 300],
    "new-parens": "error",
    "no-cond-assign": "error",
    "no-duplicate-case": "error",
    "no-duplicate-imports": "error",
    "no-empty": "warn",
    "no-empty-pattern": "warn",
    "no-eval": "error",
    "no-extra-boolean-cast": "warn",
    // "no-multiple-empty-lines": "off",
    "no-new-func": "error",
    "no-null/no-null": "error",
    "no-redeclare": "error",
    "no-sequences": "error",
    "no-trailing-spaces": ["error", { "ignoreComments": true, "skipBlankLines": true }],
    // "no-underscore-dangle": "error",
    "no-unexpected-multiline": "warn",
    "no-unused-vars": "off",
    "no-var": "error",
    "prefer-arrow/prefer-arrow-functions": "error",
    "prefer-const": "warn",
    "quote-props": ["warn", "consistent-as-needed"],
    "react/no-children-prop": "warn",
    "react/prop-types": "off", // nepouzivame
    "react-hooks/rules-of-hooks": "off", // problem je s metodami typu use***Action
    // "spaced-comment": [
    //     "warn",
    //     "always",
    //     { "markers": ["/"] }
    // ],
    "@typescript-eslint/tslint/config": [
      "warn",
      {
        "rules": {
          "completed-docs": [
            true,
            {
              "classes": {},
              "enums": { "visibilities": ["exported"] },
              "functions": { "visibilities": ["exported"] },
              "interfaces": { "visibilities": ["exported"] },
              "types": { "visibilities": ["exported"] },
              "variables": { "visibilities": ["exported"] }
            }
          ],
          "encoding": true,
          "import-spacing": true,
          "whitespace": [true, "check-branch", "check-decl", "check-operator", "check-separator", "check-type"]
        }
      }
    ]
  },
  "overrides": [
    {
      // povolit default export pro stories a slice
      "files": ["*.stories.ts", "*.stories.tsx", "*Slice.ts"],
      "rules": {
        "import/no-default-export": "off"
      }
    }
  ],
  "settings": {
    "import/resolver": {
      "node": {}, // kvuli https://github.com/benmosher/eslint-plugin-import/issues/1396#issuecomment-509384041
    },
    "react": {
      "version": "detect"
    }
  }
};