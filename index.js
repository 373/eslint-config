const path = require("path");

// kvuli https://github.com/eslint/eslint/issues/3458#issuecomment-942499561
require("@rushstack/eslint-patch/modern-module-resolution");

module.exports = require("./lib/core.js");