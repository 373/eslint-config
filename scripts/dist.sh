#!/bin/sh
mkdir -p dist
rm -rf dist/*
cp package.json dist
cp index.js dist
cp -r lib dist